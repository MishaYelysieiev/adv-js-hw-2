function HamburgerException(message) {
    this.message = message;
};

HamburgerException.prototype = Object.create(Error.prototype);

class Hamburger {
    constructor(size, stuffing) {
        this.TOPPINGS = [];
        try {
            if (!size && !stuffing) {
                throw new HamburgerException('Size and stuffing are not given!');
            }
            if (size != Hamburger.SIZE_LARGE && size != Hamburger.SIZE_SMALL) {
                throw new HamburgerException('Invalid hamburger size ' + size);
            }
            if (stuffing != Hamburger.STUFFING_CHEESE && stuffing != Hamburger.STUFFING_SALAD && stuffing != Hamburger.STUFFING_POTATO) {
                throw new HamburgerException('Invalid hamburger stuffing ' + stuffing);
            } else {
                this._size = size;
                this._stuffing = stuffing;
            }
        } catch (e) {
            console.log(e);
        }
    }

    addTopping(topping) {
        try {
            if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
                throw new HamburgerException('Invalid name of the topping!')
            }
            for (let key of this.TOPPINGS) {
                if (key.type == topping.type) {
                    throw new HamburgerException('This type of topping is already added!');
                }
            }
            console.log(topping.type + ' was added!')
            return this.TOPPINGS.push(Object.assign({}, topping));
        } catch (e) {
            console.log(e);
        }
    }

    removeTopping(topping) {
        try {
            if (topping != Hamburger.TOPPING_MAYO && topping != Hamburger.TOPPING_SPICE) {
                throw new HamburgerException('Invalid name of the topping!')
            }
            for (let key of this.TOPPINGS) {
                if (key.type == topping.type) {
                    this.TOPPINGS.splice(this.TOPPINGS.indexOf(key), 1);
                    console.log(topping.type + ' was removed!')
                } else if (this.TOPPINGS.indexOf(key) == this.TOPPINGS.length - 1) {
                    throw new HamburgerException('This type of topping was not added!');
                }
            }
        } catch (e) {
            console.log(e);
        }
    }

    get Toppings() {
        return this.TOPPINGS;
    }

    set Size(newSize) {
        try {
            if (!newSize) {
                throw new HamburgerException('Size is not given!');
            }
            if (newSize != Hamburger.SIZE_LARGE && newSize != Hamburger.SIZE_SMALL) {
                throw new HamburgerException('Invalid hamburger size ' + size);
            } else {
                return this._size = newSize;
            }
        } catch (e) {
            console.log(e);
        }
    }

    get Size() {
        return this._size;
    }

    set Stuffing(newStuffing) {
        if (!newStuffing) {
            throw new HamburgerException('Stuffing is not given!');
        }
        if (newStuffing != Hamburger.STUFFING_CHEESE && newStuffing != Hamburger.STUFFING_SALAD && newStuffing != Hamburger.STUFFING_POTATO) {
            throw new HamburgerException('Invalid hamburger stuffing ' + newStuffing);
        } else {
            this._stuffing = newStuffing;
        }
    }

    get Stuffing() {
        return this._stuffing;
    }

    calculatePrice() {
        try {
            let toppingPrice = 0;
            for (let i = 0; i < this.TOPPINGS.length; i++) {
                toppingPrice += this.TOPPINGS[i].price;
            }
            return this._size.price + this._stuffing.price + toppingPrice;
        } catch (e) {
            console.log(e);
        }
    }

    calculateCalories() {
        try {
            let toppingCalories = 0;
            for (let i = 0; i < this.TOPPINGS.length; i++) {
                toppingCalories += this.TOPPINGS[i].calories;
            }
            return this._size.calories + this._stuffing.calories + toppingCalories;
        } catch (e) {
            console.log(e);
        }
    }
};


Hamburger.SIZE_SMALL = {
    type: 'small',
    calories: 20,
    price: 50
};

Hamburger.SIZE_LARGE = {
    type: 'large',
    calories: 40,
    price: 100
};

Hamburger.STUFFING_CHEESE = {
    type: 'cheese',
    calories: 10,
    price: 20
};

Hamburger.STUFFING_SALAD = {
    type: 'salad',
    calories: 5,
    price: 20
};

Hamburger.STUFFING_POTATO = {
    type: 'potato',
    calories: 10,
    price: 15
};

Hamburger.TOPPING_MAYO = {
    type: 'mayo',
    calories: 5,
    price: 20
};

Hamburger.TOPPING_SPICE = {
    type: 'spice',
    calories: 0,
    price: 15
};

// маленький гамбургер с начинкой из сыра

const hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
// // добавка из майонеза
hamburger.addTopping(Hamburger.TOPPING_MAYO);
// // спросим сколько там калорий
console.log("Calories: %f", hamburger.calculateCalories());
// // сколько стоит
console.log("Price: %f", hamburger.calculatePrice());
// // я тут передумал и решил добавить еще приправу
hamburger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.Toppings.length); // 1
// // // А сколько теперь стоит?
console.log("Price with sauce: %f", hamburger.calculatePrice());
// // Проверить, большой ли гамбургер?
console.log("Is hamburger large: %s", hamburger.Size === Hamburger.SIZE_LARGE); // -> false
console.log("Is hamburger small: %s", hamburger.Size === Hamburger.SIZE_SMALL); // -> true
// // Убрать добавку
hamburger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", hamburger.Toppings.length); // 1
//
// // не передали обязательные параметры
// var h2 = new Hamburger(); // => HamburgerException: no size given
//
// // передаем некорректные значения, добавку вместо размера
// var h3 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE);
// // => HamburgerException: invalid size 'TOPPING_SAUCE'
//
// // добавляем много добавок
// var h4 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);
hamburger.addTopping(Hamburger.TOPPING_MAYO);